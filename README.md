# Running Exported DAGs in GCP Projects
In order to run an exported DAG in a GCP project some one time pre-processing is required.

### Pre-processing
* Create a service-account and generate a service-account JSON key.
* Provide the necessary permissions to the service-account.
* Create a cloud secret and put the service-account JSON key as the value. This secret will be used in the Roughdraft Export DAG page.
* Create a cloud storage bucket/directory to save the output jupyter-notebooks. The bucket directory URL will be used in the Roughdraft Export DAG page.
* If other GCP services are used by the workshop they need to be setup manually.
* Copy the source BigQuery tables over to the desired GCP project.

Once the pre-processing is done a workshop DAG can be exported.

### Exporting a DAG
* Go to the Export DAG page in the Roughdraft app and fill in the parameters.
* Click on the Export Dag button.
* The exported DAG file will be generated in the "healthlabs-4/exported_dags" GCS directory in healthlabs-4 GCP project. The name of the DAG file will be the same as the name of the workshop.
* Check the exported DAG file and confirm that all the parameters have been set to execute the workers in the desired GCP project.

### Running the exported DAG
* Start a Composer Environment in the GCP project.
* Upload the exported DAG file to the Composer DAGs bucket.
* The DAG will appear in the Airflow environment. It may take a couple of seconds for it to appear and the page may need to be refreshed.
* Trigger the DAG through the Airflow web-page.
* The workers will start running, the output jupyter-notebooks will be saved in the GCS directory that was configured in the Export DAG page.
