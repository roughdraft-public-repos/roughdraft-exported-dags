Field name	Type	Description	Example
EIDH	BYTES	Unique patient identifier	aXzt+K5VgEfn1dp7C2Etu
ERRegDateTimeString	STRING	STRING formatted version of ERRegDateTime	2011-12-20 2:36:00
ERRegDateTime	DATETIME	Registration timestamp in format YYYY-MM-DDThh:mm:ss	2011-12-20 2:36:00
BirthDateString	STRING	STRING formatted version of BirthDateString	1999-02-26 0:00:00
BirthDate	DATETIME	Birth timestamp in format YYYY-MM-DDThh:mm:ss	1999-02-26 0:00:00
InstitutionName	STRING	Location identifier	FSH
GenderCode	STRING	Coded gender in format F/M	F
RaceCode	STRING	Coded race	2
DiagnosisCode1	STRING	ICD9/10 code	388.7
DiagnosisCode2	STRING	ICD9/10 code	784.1
DiagnosisCode3	STRING	ICD9/10 code	789
DiagnosisCode4	STRING	ICD9/10 code	R51
DiagnosisDescription1	STRING	Description associated with DiagnosisCode1	Throat Pain
DiagnosisDescription2	STRING	Description associated with DiagnosisCode2	Syncope And Collapse
DiagnosisDescription3	STRING	Description associated with DiagnosisCode3	Headache
DiagnosisDescription4	STRING	Description associated with DiagnosisCode4	Renal Colic